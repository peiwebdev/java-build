FROM ubuntu:12.04
MAINTAINER Ed Henderson

# Install java 6
RUN apt-get update
RUN apt-get install -y openjdk-6-jdk

# Install tools
RUN apt-get install -y maven

# Define mountable directories.
VOLUME ["/dir"]

# Define working directory.
WORKDIR /dir

# Define default command.
CMD ["java"]
